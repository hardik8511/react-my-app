import React from 'react';
import { NavLink, Outlet } from 'react-router-dom'
import Header from './Header';

export default function MainHeader() {
    return (
        <div>
            <Header />
            {/* <h1>Welcome To The Home</h1> */}
            <Outlet />
        </div>
    )
}
