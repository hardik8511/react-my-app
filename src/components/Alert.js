import React from "react";

export default function Alert(props) {
    // console.log(props.alertMsg);
    return (
        props.alertMsg && <div className={`alert alert-${props.alertMsg.type} alert-dismissible fade show`} role="alert">
            <strong> {props.alertMsg.msg} </strong> {props.alertMsg.type}
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    );
}
