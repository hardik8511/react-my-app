import React, { useState } from "react";

export default function TextForm(props) {
  const hanldeUpClick = () => {
    // console.log("upper case clicked");
    // setText("you have click on handle upper click");
    // console.log(text.toUpperCase());
    let toUpper = text.toUpperCase();
    setText(toUpper);
  };
  const handleOnchange = (event) => {
    setText(event.target.value);
    console.log("on change event click");
  };
  const convertLoverCase = () => {
    let toLowerCase = text.toLowerCase();
    setText(toLowerCase);
  };

  const handleClearText = () => {
    setText("");
  };

  const [text, setText] = useState("Enter The Text Here Hardik");
  return (
    <div>
      <div
        className="form-group"
        style={{ color: props.mode === "light" ? "black" : "white" }}
      >
        <h3> {props.heading}</h3>
        <textarea style={{ color: props.mode === "light" ? "black" : "white" }} type="textarea" className="form-control" id="myText" value={text} aria-describedby="emailHelp" onChange={handleOnchange} placeholder="Enter email"
        ></textarea>
        <button className="btn btn-primary my-3" onClick={hanldeUpClick}>
          Convert To Upper Case
        </button>
        <button
          className="btn btn-primary my-3 mx-1"
          onClick={convertLoverCase}
        >
          Convert To Lower Case
        </button>
        <button className="btn btn-primary my-3 mx-1" onClick={handleClearText}>
          Clear Text
        </button>
      </div>

      <div>
        <h2>text summary</h2>
        <p>
          In your input words is {text.split(" ").length} and your character is{" "}
          {text.length}{" "}
        </p>
        Preview : <h2>{text}</h2>
      </div>
    </div>
  );
}
