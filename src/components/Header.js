import React from 'react'
import { NavLink } from 'react-router-dom'

export default function Header() {
    return (
        <header>
            
            <nav>
                <ul>
                    <li> <a href='#'>Logo</a>   </li>
                    <li> <NavLink  to="/home" > Home  </NavLink>   </li>
                    <li> <NavLink to="/about" >About</NavLink>   </li>
                    <li> <NavLink to="/contact" >Contact</NavLink>  </li>
                </ul>
            </nav>
        </header>

    )
}
