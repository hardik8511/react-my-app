// import logo from "./logo.svg";
import "./App.css";
import About from "./components/About";
import Navbar from "./components/Navbar";
import TextForm from "./components/TextForm";
import React, { useState } from "react";
import Alert from "./components/Alert";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Contact from "./components/contact";
import Home from "./components/Home";
import Error from "./components/Error";
import MainHeader from './components/MainHeader'
// let full_name = "hardik kanzariya";
function App() {
  const [darkMode, setDarkMode] = useState("light");
  const [alertMsg, setAlert] = useState(null); //this is called the state 

  const displayAlertMsg = (message, type) => {
    setAlert({
      msg: message,
      type: type,
    });
    setTimeout(() => {
      setAlert("");
    }, 4000);
  }

  //function for the toggle mode
  const toggleMode = () => {
    console.log(darkMode);
    if (darkMode == "light") {
      setDarkMode("dark");
      document.body.style.backgroundColor = "grey";
      displayAlertMsg("dark mode", "success");

    } else {
      setDarkMode("light");
      document.body.style.backgroundColor = "white";
      displayAlertMsg("right now light mode enable", "success");
    }
  };

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Navbar title="Hello TextUtils" aboutUs="About Us" contactUs="Contact" toggleMode={toggleMode} mode={darkMode} />}>
            <Route path="/home" element={<TextForm heading="Enter The Text To The Analyse" mode={darkMode} />}>About</Route>
            <Route path="/about" element={<About />}>About</Route>
            <Route path="/contact" element={<Contact />}>About</Route>
            <Route path="*" element={<Error />}>About</Route>
          </Route>
        </Routes>
      </BrowserRouter>
      {/* <Navbar title="Hello TextUtils" aboutUs="About Us" contactUs="Contact" toggleMode={toggleMode} mode={darkMode} />
      <Alert alertMsg={alertMsg} />
      <TextForm heading="Enter The Text To The Analyse" mode={darkMode} /> */}
      {/* route demo  */}

      {/* routing demo start */}
      {/* <BrowserRouter>
        <Routes>
          <Route path="/" element={<MainHeader />} >
            <Route path="/home" element={<Home />} ></Route>
            <Route path="/about" element={<About />} ></Route>
            <Route path="/contact" element={<Contact />} ></Route>
            <Route path="*" element={<Error />} ></Route>
          </Route>
        </Routes>
      </BrowserRouter> */}
      {/* routing demo end  */}
    </>
  );
  // return <BrowserRouter>
  //   <Routes>
  //     <Route path="/" element={<div> Home Page </div>}></Route>
  //     <Route path="/about" element={<div> Home Page </div>}></Route>
  //   </Routes>
  // </BrowserRouter >
}

export default App;
